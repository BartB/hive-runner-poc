USE ${hiveconf:data_base};



DROP TABLE IF EXISTS ${hiveconf:data_base}.average_verb_frequency;

CREATE TABLE ${hiveconf:data_base}.average_verb_frequency AS
SELECT
    stg.first_letters,
    AVG(stg.verb_frequency)  AS average_verb_frequency
FROM (
    SELECT
        SUBSTR(it.first_word_in_chapter, 1, 2) AS first_letters,
        t1.*  
    FROM 
        ${hiveconf:data_base}.chapters it,
        ${hiveconf:data_base}.chapter_verbs t1
    WHERE
        it.title = t1.title AND
        it.chapter_num = t1.chapter_num AND
        t1.verb_tense = 'PAST' AND
        t1.verb_frequency < 100
) stg
GROUP BY first_letters
;



