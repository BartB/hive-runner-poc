-- Create test database:
CREATE DATABASE ${hiveconf:data_base};

-- Create structure for input tables:
-- The data will be loaded from CSV files
CREATE TABLE ${hiveconf:data_base}.chapter_verbs(
    title           STRING,
    chapter_num     INT,
    verb_tense      STRING,
    verb_frequency  INT
);

CREATE TABLE ${hiveconf:data_base}.chapters(
    title                   STRING,
    chapter_num             INT,
    first_word_in_chapter   STRING
);

-- Create structure for expected results table:
-- The data will be loaded from CSV file
CREATE TABLE ${hiveconf:data_base}.expected_result(
    first_letters           STRING,
    average_verb_frequency  DOUBLE
);
