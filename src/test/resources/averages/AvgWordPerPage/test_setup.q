-- Create test database:
CREATE DATABASE ${hiveconf:data_base};

-- Create structure for input tables:
-- The data will be loaded from CSV files
CREATE TABLE ${hiveconf:data_base}.books(
    title       STRING,
    font_size   INT
);

CREATE TABLE ${hiveconf:data_base}.chapter_word_counts(
    title           STRING,
    chapter_num     INT,
    the_word        STRING,
    word_count      INT
);

CREATE TABLE ${hiveconf:data_base}.chapters(
    title           STRING,
    chapter_num     INT,
    font_style      STRING,
    num_of_pages    DOUBLE
);

-- Create structure for expected results table:
-- The data will be loaded from CSV file
CREATE TABLE ${hiveconf:data_base}.expected_result(
    font_size           INT,
    font_style          STRING,
    total_num_pages     DOUBLE,
    total_word_count    INT,
    avg_word_per_page   DOUBLE
);
