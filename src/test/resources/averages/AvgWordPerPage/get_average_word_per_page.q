USE ${hiveconf:data_base};



DROP TABLE IF EXISTS ${hiveconf:data_base}.average_word_per_page;

CREATE TABLE ${hiveconf:data_base}.average_word_per_page AS
SELECT
    book.font_size,
    chtr.font_style,
    SUM(chtr.num_of_pages)                      AS total_num_pages,
    SUM(wdct.word_count)                        AS total_word_count,
    SUM(chtr.num_of_pages) / SUM(wdct.word_count) AS avg_word_per_page
FROM ${hiveconf:data_base}.chapters chtr
LEFT JOIN ${hiveconf:data_base}.chapter_word_counts wdct
ON 
    chtr.title = wdct.title AND
    chtr.chapter_num = wdct.chapter_num
LEFT JOIN ${hiveconf:data_base}.books book
ON 
    chtr.title = book.title
WHERE
    book.font_size IS NOT NULL AND
    chtr.num_of_pages > 1 AND
    wdct.word_count < 100000 AND
    wdct.the_word = 'BUT'
GROUP BY
    book.font_size,
    chtr.font_style
;



