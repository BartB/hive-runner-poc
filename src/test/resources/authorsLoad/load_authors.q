USE ${hiveconf:data_base};


--------------------------------------------------------------------

DROP TABLE IF EXISTS ${hiveconf:data_base}.authors_tmp;

CREATE TABLE ${hiveconf:data_base}.authors_tmp AS
SELECT 
    stg.author_id,
    stg.age,
    stg.extra_identifier,
    stg.change_date,
    CASE
        WHEN stg.penname = '' THEN NULL
        ELSE stg.penname
    END AS penname,
    ROW_NUMBER() OVER (PARTITION BY author_id ORDER BY change_date) AS pennameUpdateSequence
FROM (
    SELECT 
        author_id,
        age,
        extra_identifier,
        change_date,
        TRIM(penname_id)   AS penname
    FROM ${hiveconf:data_base}.authors
    UNION ALL
    SELECT 
        author_id,
        age,
        extra_identifier,
        change_date,
        TRIM(penname_id)   AS penname
    FROM ${hiveconf:other_data_base}.authors_updates
) stg
;



--------------------------------------------------------------------

DROP TABLE IF EXISTS ${hiveconf:data_base}.authors_pennames;

CREATE TABLE ${hiveconf:data_base}.authors_pennames AS
SELECT 
    curr_updt.author_id, 
    curr_updt.age,
    curr_updt.extra_identifier,
    curr_updt.penname,
    curr_updt.change_date AS used_from,
    CASE
        WHEN next_updt.change_date IS NULL THEN 99991231.235959999
        ELSE next_updt.change_date
    END AS used_until
FROM ${hiveconf:data_base}.authors_tmp curr_updt
LEFT JOIN ${hiveconf:data_base}.authors_tmp next_updt
ON
    curr_updt.author_id = next_updt.author_id AND
    curr_updt.extra_identifier = next_updt.extra_identifier AND
    curr_updt.pennameUpdateSequence = next_updt.pennameUpdateSequence - 1
;


DROP TABLE IF EXISTS ${hiveconf:data_base}.authors_tmp;


--------------------------------------------------------------------

DROP TABLE IF EXISTS ${hiveconf:data_base}.recent_author_penname;

CREATE TABLE ${hiveconf:data_base}.recent_author_penname AS
SELECT 
    tb1.author_id,
    tb1.extra_identifier,
    stg.penname,
    stg.used_from,
    tb1.used_until
FROM (
    SELECT
        penname, 
        MAX(used_from)     AS used_from
    FROM ${hiveconf:data_base}.authors_pennames
    WHERE penname IS NOT NULL
    GROUP BY penname
) stg
JOIN ${hiveconf:data_base}.authors_pennames tb1
ON
    stg.penname = tb1.penname AND
    stg.used_from = tb1.used_from
;



