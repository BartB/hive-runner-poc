-- Create test database:
CREATE DATABASE ${hiveconf:data_base};

-- Create structure for input tables:
-- The data will be loaded from CSV files
CREATE TABLE ${hiveconf:data_base}.authors(
    author_id           STRING,
    age                 INT,
    extra_identifier    INT,
    change_date         INT,
    penname_id          STRING
);

-- Create test database:
CREATE DATABASE ${hiveconf:other_data_base};

-- Create structure for input tables:
-- The data will be loaded from CSV files
CREATE TABLE ${hiveconf:other_data_base}.authors_updates(
    author_id           STRING,
    age                 INT,
    extra_identifier    INT,
    change_date         INT,
    penname_id          STRING
);

-- Create structure for expected results table:
-- The data will be loaded from CSV file
CREATE TABLE ${hiveconf:data_base}.expected_result(
    author_id           STRING,
    extra_identifier    INT,
    penname             STRING,
    used_from           DOUBLE,
    used_until          DOUBLE
);
