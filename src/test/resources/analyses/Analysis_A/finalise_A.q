USE ${hiveconf:data_base};



DROP TABLE IF EXISTS ${hiveconf:data_base}.results_A;

CREATE TABLE ${hiveconf:data_base}.results_A AS
SELECT
    res.result_type,
    res.title,
    res.book_author_penname,
    7   AS value_a, 
    7   AS value_b, 
    7   AS value_c, 
    0.0     AS value_d,
    CAST(null as DOUBLE)    AS value_e
FROM ${hiveconf:data_base}.result_A_detail res
;





