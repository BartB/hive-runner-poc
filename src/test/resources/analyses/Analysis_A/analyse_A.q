USE ${hiveconf:data_base};



DROP TABLE IF EXISTS ${hiveconf:data_base}.result_A_detail;

CREATE TABLE ${hiveconf:data_base}.result_A_detail AS
SELECT DISTINCT
    'RES_A' AS result_type,
    title,
    published_date,
    genre,
    book_format,
    lib_section,
    book_author_penname,
    book_author_id,
    is_author_worth_reading
FROM ${hiveconf:data_base}.books 
WHERE
    genre IN ('Crime', 'SciFi') AND
    book_format IN ('Paperback', 'HardCover') AND
    lib_section = 'A' AND
    SUBSTR(UPPER(book_author_penname), 1, 3) NOT IN ('STE', 'JOH', 'ANN', 'NIA', 'BER') AND
    UPPER(is_author_worth_reading) = 'Y'
;





