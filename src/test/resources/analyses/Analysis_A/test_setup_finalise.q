-- Create test database:
CREATE DATABASE ${hiveconf:data_base};

-- Create structure for input tables:
-- The data will be loaded from CSV files
CREATE TABLE ${hiveconf:data_base}.result_A_detail(
    result_type     STRING,
    title           STRING,
    published_date  INT,
    genre           STRING,
    book_format     STRING,
    lib_section     STRING,
    book_author_penname     STRING,
    book_author_id          INT,
    is_author_worth_reading STRING
);

-- Create structure for expected results table:
-- The data will be loaded from CSV file
CREATE TABLE ${hiveconf:data_base}.expected_result(
    result_type     STRING,
    title           STRING,
    book_author_penname STRING,
    value_a     INT,
    value_b     INT,
    value_c     INT,
    value_d     DOUBLE,
    value_e     DOUBLE
);
