-- Create test database:
CREATE DATABASE ${hiveconf:data_base};

-- Create structure for input tables:
-- The data will be loaded from CSV files
CREATE TABLE ${hiveconf:data_base}.chapters(
    title           STRING,
    chapter_num     INT,
    weight          INT,
    first_word_in_chapter   STRING,
    price         DOUBLE
);

CREATE TABLE ${hiveconf:data_base}.filter_01(
    title   STRING
);

CREATE TABLE ${hiveconf:data_base}.books(
    title               STRING,
    published_date      INT,
    genre               STRING,
    book_format         STRING,
    lib_section         STRING,
    book_author_penname STRING
);

-- Create structure for expected results table:
-- The data will be loaded from CSV file
CREATE TABLE ${hiveconf:data_base}.expected_result(
    result_type         STRING,
    title               STRING,
    chapter_num         INT,
    published_date      INT,
    genre               STRING,
    book_format         STRING,
    lib_section         STRING,
    book_author_penname STRING,
    weight              INT
);
