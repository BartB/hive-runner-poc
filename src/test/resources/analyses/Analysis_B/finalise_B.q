USE ${hiveconf:data_base};


--------------------------------------------------------------------

DROP TABLE IF EXISTS ${hiveconf:data_base}.result_b_vb_info;

CREATE TABLE ${hiveconf:data_base}.result_b_vb_info AS
SELECT
    t1.title,
    t1.chapter_num,
    vb.verb_tense,
    vb.verb_frequency,
    vb.verb_count
FROM ${hiveconf:data_base}.result_B_detail t1
LEFT JOIN ${hiveconf:data_base}.chapter_verbs vb
ON
    vb.title = t1.title AND
    vb.chapter_num = t1.chapter_num AND
    vb.verb_tense = 'PAST'
;

--------------------------------------------------------------------

DROP TABLE IF EXISTS ${hiveconf:data_base}.result_b_val_d;

CREATE TABLE ${hiveconf:data_base}.result_b_val_d AS
SELECT
    t1.title,
    t1.chapter_num,
    CASE
        WHEN t1.value_d > 0 THEN t1.value_d
        ELSE 0.00
    END AS value_d
FROM (
    SELECT
        rb.title,
        rb.chapter_num,
        CASE
            WHEN rb.weight <= 99000
                THEN (rb.weight * 3.50 * vb.verb_frequency / 100) - vb.verb_count
            ELSE (50000 * 3.50 * vb.verb_frequency / 100) - vb.verb_count
        END AS value_d
    FROM
        ${hiveconf:data_base}.result_B_detail rb,
        ${hiveconf:data_base}.result_b_vb_info vb
    WHERE
        vb.title = rb.title AND
        vb.chapter_num = rb.chapter_num 
    ) t1
;

--------------------------------------------------------------------

DROP TABLE IF EXISTS ${hiveconf:data_base}.results_B;

CREATE TABLE ${hiveconf:data_base}.results_B AS
SELECT
    rb.result_type,
    rb.title,
    rb.chapter_num,
    rb.book_author_penname,
    8 AS value_a, 
    3 AS value_b, 
    6 AS value_c, 
    vd.value_d,
    CAST(null as DOUBLE) AS value_e
FROM 
    ${hiveconf:data_base}.result_B_detail rb,
    ${hiveconf:data_base}.result_b_val_d vd
WHERE
    rb.title = vd.title AND
    rb.chapter_num = vd.chapter_num 
;





