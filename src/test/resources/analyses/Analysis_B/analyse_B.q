USE ${hiveconf:data_base};

--------------------------------------------------------------------

DROP TABLE IF EXISTS ${hiveconf:data_base}.results_b_condition_1;

CREATE TABLE ${hiveconf:data_base}.results_b_condition_1 AS
SELECT 
    title, 
    chapter_num
FROM ${hiveconf:data_base}.chapters
WHERE 
    weight > 0 AND
    weight <= 99000 AND
    SUBSTR(first_word_in_chapter, 1, 2) NOT IN ( 'Ve', 'Lo', 'Ci', 'Ty') AND
    price / weight < 0.03
;

--------------------------------------------------------------------

DROP TABLE IF EXISTS ${hiveconf:data_base}.results_b_condition_2;

CREATE TABLE ${hiveconf:data_base}.results_b_condition_2 AS
SELECT 
    title, 
    chapter_num
FROM ${hiveconf:data_base}.chapters
WHERE 
    weight > 99000 AND
    SUBSTR(first_word_in_chapter, 1, 2) NOT IN ( 'Ve', 'Lo', 'Ci', 'Ty') AND
    price / 50000 < 0.03
;

--------------------------------------------------------------------

DROP TABLE IF EXISTS ${hiveconf:data_base}.results_b_chapters;

CREATE TABLE ${hiveconf:data_base}.results_b_chapters AS
SELECT 
    'RES_B' AS result_type, 
    t1.title, 
    t1.chapter_num
FROM (
    SELECT * FROM ${hiveconf:data_base}.results_b_condition_1 con1
    UNION ALL
    SELECT * FROM ${hiveconf:data_base}.results_b_condition_2 con2
) t1
INNER JOIN ${hiveconf:data_base}.filter_01 fil
ON
    t1.title = fil.title
;

--------------------------------------------------------------------

DROP TABLE IF EXISTS ${hiveconf:data_base}.result_B_detail;

CREATE TABLE ${hiveconf:data_base}.result_B_detail AS
SELECT DISTINCT
    tb.result_type,
    tb.title,
    tb.chapter_num,
    bk.published_date,
    bk.genre,
    bk.book_format,
    bk.lib_section,
    bk.book_author_penname,
    cp.weight
FROM 
    ${hiveconf:data_base}.results_b_chapters tb,
    ${hiveconf:data_base}.books bk,
    ${hiveconf:data_base}.chapters cp
WHERE 
    tb.title = bk.title AND
    tb.title = cp.title AND
    tb.chapter_num = cp.chapter_num
;


