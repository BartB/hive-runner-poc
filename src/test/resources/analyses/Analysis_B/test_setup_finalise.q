-- Create test database:
CREATE DATABASE ${hiveconf:data_base};

-- Create structure for input tables:
-- The data will be loaded from CSV files
CREATE TABLE ${hiveconf:data_base}.result_B_detail(
    result_type     STRING,
    title           STRING,
    chapter_num     INT,
    published_date  INT,
    genre           STRING,
    book_format     STRING,
    lib_section     STRING,
    book_author_penname     STRING,
    weight          INT
);

CREATE TABLE ${hiveconf:data_base}.chapter_verbs(
    title           STRING,
    chapter_num     INT,
    verb_tense      STRING,
    verb_frequency  INT,
    verb_count      DOUBLE
);

-- Create structure for expected results table:
-- The data will be loaded from CSV file
CREATE TABLE ${hiveconf:data_base}.expected_result(
    result_type     STRING,
    title           STRING,
    chapter_num     INT,
    book_author_penname STRING,
    value_a     INT,
    value_b     INT,
    value_c     INT,
    value_d     DOUBLE,
    value_e     DOUBLE
);
