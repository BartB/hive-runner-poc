USE ${hiveconf:data_base};

--------------------------------------------------------------------


DROP TABLE IF EXISTS ${hiveconf:data_base}.ref_books_list_ext;

CREATE EXTERNAL TABLE ${hiveconf:data_base}.ref_books_list_ext(
    ref_no  STRING,
    title   STRING
)
COMMENT 'External table to hold books data from books_list.csv'
ROW FORMAT DELIMITED
FIELDS TERMINATED BY ','
LOCATION '${hiveconf:hdfs_ext_dir}/ref_books_list_ext'
-- The file has a header row - ignore it:
TBLPROPERTIES ("skip.header.line.count"="1")
;



--------------------------------------------------------------------


DROP TABLE IF EXISTS ${hiveconf:data_base}.ref_books_list;

CREATE TABLE ${hiveconf:data_base}.ref_books_list AS
SELECT
    ref_no,
    title
FROM ${hiveconf:data_base}.ref_books_list_ext
;

