-- Create test database:
CREATE DATABASE ${hiveconf:data_base};

-- Create structure for expected results table:
-- The data will be loaded from CSV file
CREATE TABLE ${hiveconf:data_base}.expected_result(
    ref_no  STRING,
    title   STRING
);
