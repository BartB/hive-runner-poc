package testcases.averages;

import com.klarna.hiverunner.HiveRunnerExtension;
import com.klarna.hiverunner.HiveShell;
import com.klarna.hiverunner.annotations.HiveSQL;
import com.klarna.hiverunner.data.TsvFileParser;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;

import java.io.File;
import java.nio.file.Paths;
import java.util.List;

import static org.junit.jupiter.api.Assertions.assertEquals;


/*
 * POC test case that:
 * - creates test database with table structures
 * - loads input data into these tables from csv files
 * - executes the HiveQL script under test
 *      - single query
 *      - single result table
 * - verifies the values in the result table
 */
@ExtendWith(HiveRunnerExtension.class)
public class GetAverageVerbFrequencyTest {

    /*
     * Constants for the test - just to make things easier
     */
    private static final String DB_NAME = "test_db";
    private static final String TEST_RESOURCE_ROOT = "src/test/resources/averages/AvgVerbFrequency/";

    /*
     * This field with its annotation is mandatory (though it does not have to list any files).
     * The HiveRunner will set the HiveShell instance before each test method is called.
     *
     * With @HiveSQL we can define the script files under test, but here we will run them
     * in out @Test method (this seems to be more reliable on Windows).
     *
     * We do not start the shell just yet (autoStart=false). Shell needs to be started
     * after all hiveconf properties required by the test have been set. We do this in
     * @BeforeEach method.
     */
    @HiveSQL(files = {}, autoStart = false)
    private HiveShell shell;


    /*
     * As with regular JUnit5 tests, we can use all JUnit annotations.
     * Doing initialisation in @BeforeEach is more reliable on Windows.
     */
    @BeforeEach
    public void setupDatabase() {

        /*
         * Set all hiveconf properties required by the test case.
         * MUST cater for all the parameters in the scripts that we want to test.
         */
        // database name: the key MUST match what's used in the tested scripts e.g. in USE ${hiveconf:data_base}
        shell.setHiveConfValue("data_base",DB_NAME);
//        String actualBaseDir = shell.getBaseDir().toString();
//        hiveProperties.put("input_data_root", actualBaseDir);
//        shell.getHiveConf().getAllProperties().setProperty("input_data_root", actualBaseDir);

        /*
         * Start the shell now:
         */
        shell.start();

        /*
         * Call set-up script - it creates databases and table structures required by the test case:
         */
        shell.execute(Paths.get(TEST_RESOURCE_ROOT + "test_setup.q"));

        /*
         * Load test input data from CSVs into tables created in setup script:
         */
        // First instantiate parser to parse the CSVs. In our specific case the same parser
        // is used for all the files.
        TsvFileParser parser = new TsvFileParser()
                .withHeader()
                .withDelimiter(",")
                .withNullValue("");

        // Now load one-by-one:
        File inputCsvFile = new File(TEST_RESOURCE_ROOT + "chapter_verbs.csv");
        shell.insertInto(DB_NAME, "chapter_verbs")
                .withAllColumns()
                .addRowsFrom(inputCsvFile, parser)
                .commit();

        inputCsvFile = new File(TEST_RESOURCE_ROOT + "chapters.csv");
        shell.insertInto(DB_NAME, "chapters")
                .withAllColumns()
                .addRowsFrom(inputCsvFile, parser)
                .commit();

        /*
         * Load test expected output data from CSVs into expected_result table
         * created in setup script:
         */
        inputCsvFile = new File(TEST_RESOURCE_ROOT + "average_verb_frequency.csv");
        shell.insertInto(DB_NAME, "expected_result")
                .withAllColumns()
                .addRowsFrom(inputCsvFile, parser)
                .commit();

    }


    /*
     * Execute the test:
     */
    @Test
    public void testGetAverageVerbFrequency() {
        /*
         * Run the HiveQL script under test:
         */
        shell.execute(Paths.get(TEST_RESOURCE_ROOT + "get_average_verb_frequency.q"));

        // Optionally, print result for debugging purposes:
        System.out.println(shell.executeQuery("select * from average_verb_frequency"));

        /*
         * Verify the result:
         * To be able to compare the actual result to the expected one we need to:
         * - sort the actual and expected table in the same way
         * - verify that the tables have the same amount of rows
         * - loop through the expected rows and verify that each matches the actual
         */

        // Sort the actual and expected table in the same way:
        List<Object[]> expectedResult = shell.executeStatement(new StringBuilder()
                .append("select * from expected_result order by ")
                .append("first_letters")
                .toString());

        List<Object[]> actualResult = shell.executeStatement(new StringBuilder()
                .append("select * from ")
                .append("average_verb_frequency")
                .append(" order by ")
                .append("first_letters")
                .toString());

        // Verify that the tables have the same amount of rows:
        assertEquals(expectedResult.size(), actualResult.size(), "Assert result size");

        // Loop through the expected rows and verify that each matches the actual:
        for (int i = 0; i < expectedResult.size(); i++) {
            // Since we've ordered the tables in the same way rows at the same index are expected to match:
            Object[] expectedRow = expectedResult.get(i);
            Object[] actualRow = actualResult.get(i);

            // For all columns assert their values match:
            // These assertions need to be re-written for each test and each table.
            assertEquals(expectedRow[0],actualRow[0], "Assert result for row idx [" + i +"], first_letters");
            assertEquals(expectedRow[1],actualRow[1], "Assert result for row idx [" + i +"], average_verb_frequency");
        }

    }


}
