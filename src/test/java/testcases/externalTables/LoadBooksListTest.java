package testcases.externalTables;

import com.klarna.hiverunner.HiveRunnerExtension;
import com.klarna.hiverunner.HiveShell;
import com.klarna.hiverunner.annotations.HiveSQL;
import com.klarna.hiverunner.data.TsvFileParser;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;

import java.io.File;
import java.nio.file.Paths;
import java.util.List;

import static org.junit.jupiter.api.Assertions.assertEquals;


/*
 * POC test case that:
 * - creates test database and HDFS location for external tables
 * - loads input data from csv file into HDFS
 * - executes the HiveQL script under test
 *      - first query creates external table from the input data
 *      - second query creates staging result table from the external one
 * - verifies the values in the result table
 */
@ExtendWith(HiveRunnerExtension.class)
public class LoadBooksListTest {

    /*
     * Constants for the test - just to make things easier
     */
    private static final String DB_NAME = "test_db";
    private static final String TEST_RESOURCE_ROOT = "src/test/resources/externalTables/";

    /*
     * This field with its annotation is mandatory (though it does not have to list any files).
     * The HiveRunner will set the HiveShell instance before each test method is called.
     *
     * With @HiveSQL we can define the script files under test, but here we will run them
     * in out @Test method (this seems to be more reliable on Windows).
     *
     * We do not start the shell just yet (autoStart=false). Shell needs to be started
     * after all hiveconf properties required by the test have been set. We do this in
     * @BeforeEach method.
     */
    @HiveSQL(files = {}, autoStart = false)
    private HiveShell shell;


    /*
     * As with regular JUnit5 tests, we can use all JUnit annotations.
     * Doing initialisation in @BeforeEach is more reliable on Windows.
     */
    @BeforeEach
    public void setupDatabase() {

        /*
         * New test base dir path is generated each time the test is run.
         * We keep reference to it as it'll be needed later during the setUp steps.
         * NOTE: the path does not end with "\".
         */
        // This is the Windows style path (dirs separated by "\" ):
        String testBaseDirWinStyle = shell.getBaseDir().toString();
        // This is the Unix style path (dirs separated by "/" ):
        String testBaseDirUnixStyle = testBaseDirWinStyle.replace("\\", "/");


        /*
         * Set all hiveconf properties required by the test case.
         * MUST cater for all the parameters in the scripts that we want to test.
         */
        // Database name: the key MUST match what's used in the tested scripts e.g. in USE ${hiveconf:data_base}
        shell.setHiveConfValue("data_base", DB_NAME);
        // Directory to act as the HDFS root location for the external table's data.
        // - The key MUST match what's used in the tested scripts e.g in LOCATION '${hiveconf:hdfs_ext_dir}...'
        // - The value MUST be the test base dir path in Unix style!
        shell.setHiveConfValue("hdfs_ext_dir", testBaseDirUnixStyle);


        /*
         * Load test input data from CSVs into HDFS directory. This step MUST be done before the shell is started!
         * The target file MUST be subdir of the test base dir but this time in Windows style
         * and MUST be what's declared (in Unix style) in the script under test + the file name:
         * LOCATION '${hiveconf:hdfs_ext_dir}/ref_books_list_ext'
         */
        File inputCsvFile = new File(TEST_RESOURCE_ROOT + "books_list.csv");
        shell.addResource(testBaseDirWinStyle + "\\ref_books_list_ext\\books_list.csv", inputCsvFile);


        /*
         * Start the shell now:
         */
        shell.start();


        /*
         * Call set-up script - it creates databases and table structures required by the test case:
         */
        shell.execute(Paths.get(TEST_RESOURCE_ROOT + "test_setup.q"));


        /*
         * Load test expected output data from CSVs into expected_result table
         * created in setup script:
         */

        // First instantiate parser to parse the CSVs:
        TsvFileParser parser = new TsvFileParser()
                .withHeader()
                .withDelimiter(",")
                .withNullValue("");

        inputCsvFile = new File(TEST_RESOURCE_ROOT + "ref_books_list.csv");
        shell.insertInto(DB_NAME, "expected_result")
                .withAllColumns()
                .addRowsFrom(inputCsvFile, parser)
                .commit();

    }


    /*
     * Execute the test:
     */
    @Test
    public void testLoadBooksList() {
        /*
         * Run the HiveQL script under test:
         */
        shell.execute(Paths.get(TEST_RESOURCE_ROOT + "load_books_list.q"));

        /*
         * Verify the result:
         * To be able to compare the actual result to the expected one we need to:
         * - sort the actual and expected table in the same way
         * - verify that the tables have the same amount of rows
         * - loop through the expected rows and verify that each matches the actual
         */

        // Sort the actual and expected table in the same way:
        List<Object[]> expectedResult = shell.executeStatement(new StringBuilder()
                .append("select * from expected_result order by ")
                .append("ref_no, title")
                .toString());

        List<Object[]> actualResult = shell.executeStatement(new StringBuilder()
                .append("select * from ")
                .append("ref_books_list")
                .append(" order by ")
                .append("ref_no, title")
                .toString());

        // Verify that the tables have the same amount of rows:
        assertEquals(expectedResult.size(), actualResult.size(), "Assert result size");

        // Loop through the expected rows and verify that each matches the actual:
        for (int i = 0; i < expectedResult.size(); i++) {
            // Since we've ordered the tables in the same way rows at the same index are expected to match:
            Object[] expectedRow = expectedResult.get(i);
            Object[] actualRow = actualResult.get(i);

            // For all columns assert their values match:
            // These assertions need to be re-written for each test and each table.
            assertEquals(expectedRow[0],actualRow[0], "Assert result for row idx [" + i +"], ref_no");
            assertEquals(expectedRow[1],actualRow[1], "Assert result for row idx [" + i +"], title");
        }

    }

}
